# --------------------------Exercicio 1 -----------------------------
primeiroNumero = int(input('Digite um numero inteiro qualquer: '))
segundoNumero = int(input('Digite outro numero inteiro qualquer: '))

if primeiroNumero > segundoNumero:
    print(f"O número  {primeiroNumero} é maior que {segundoNumero}")
else:
    print(f"O número  {segundoNumero} é maior que {primeiroNumero}")

# --------------------------Exercicio 2 -----------------------------

crescimentoDaEmpresaNoMesPassado = float(input('Digite o percentual de crescimento da empresa no mês passado: '))
crescimentoDaEmpresaNoMes = float(input('Digite o percentual de crescimento da empresa neste mês: '))

if crescimentoDaEmpresaNoMesPassado > crescimentoDaEmpresaNoMes:
    print(f"Houve um decrescimento na empresa, em comparação ao mês anterior")
else:
    print(f"Houve um crescimento na empresa, em comparação ao mês anterior")