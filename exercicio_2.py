# --------------------------Exercicio 1 -----------------------------
primeiroNumero = int(input('Digite um numero inteiro qualquer: '))
segundoNumero = int(input('Digite outro numero inteiro qualquer: '))

cont = primeiroNumero + 1

print(primeiroNumero)

for cont in range(cont, segundoNumero):
    print(cont)

print(segundoNumero)

# --------------------------Exercicio 2 -----------------------------

tabuada = int(input('Digite um numero inteiro para a tabuada: '))

cont = 1

for cont in range(cont, 11):
    operaçaoTabuada = tabuada * cont
    print(f'{cont} X {tabuada} = {operaçaoTabuada}')