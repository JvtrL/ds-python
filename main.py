# print("João", 29)

# nome = input('Qual o seu nome? ')
# print(nome)

# ano_entrada = int(input('Escreva o ano de ingresso do estudante: '))
# print(type(ano_entrada))

# nota = float(input('Escreva a nota do aluno: '))
# print(nota)

# media = float(input('Digite a media do aluno: '))
#
# if media>=6.0:
#     print("Aluno aprovado!")
# elif 6.0 > media >= 4.0:
#     print("NP3")
# else:
#     print("Reprovado")

# t1 = t2 = True
# f1 = f2 = False
#
# if t1 and f2:
#     print('expressao verdadeira')
# else:
#     print('expressao falsa')
#
# if t1 or f2:
#     print('expressao verdadeira')
# else:
#     print('expressao falsa')
#
# if not f1:
#     print('expressao verdadeira')
# else:
#     print('expressao falsa')

# lista = 'Leandro, Guilherme, João, Pedro, Ana'
#
# nome_1 = 'Leandro'
# nome_2 = 'Guilherme'
# nome_3 = 'Guilherme'
# nome_4 = 'Pedro'
# nome_5 = 'Ana'
#
# if nome_1 in lista:
#     print(f"{nome_1} está na lista")
# else:
#     print(f"{nome_1} não está na lista")

# cont = 1
#
# while cont <=3:
#     nota_1 = float(input('Digite a primeira nota: '))
#     nota_2 = float(input('Digite a segunda nota: '))
#
#     print(f"Media: {(nota_1 + nota_2) / 2}")
#
#     cont += 1

# for cont in range(1,11):
#     print(cont)
#
# for cont in range(1,4):
#     nota_1 = float(input('Digite a primeira nota: '))
#     nota_2 = float(input('Digite a segunda nota: '))
#
#     print(f"Media: {(nota_1 + nota_2) / 2}")

# lista = ["Pivas", 9.5, 9.0, 8.0, True]
# print(lista)
#
# print(lista[0])
#
# for elemento in lista:
#     print(elemento)

# lista[3] = 10
#
# for elemento in lista:
#     print(elemento)

# media = (lista[1] + lista[2] + lista[3]) / 3
# print(f"Média: {media}")

# print(len(lista)) #conta os itens dentro da lista
#
# lista.append(media) # adiciona um elmento ao final da lsita
# print(lista)

# lista.extend([10.0, 8.5, 9.0]) # adiciona varios elementos ao final
# print(lista)
#
# lista.remove("Pivas")
# print(lista)

# dicionario = {
#     'chave1': 1,
#     'chave2': 2,
#     'chave3': 3
# }

# print(dicionario)

# cadastro = {
#     'matricula': 1234,
#     'dia_cadastro': 32,
#     'mes_cadastro': 13,
#     'turma': '3G'
# }
#
# print(cadastro['matricula'])
# print(cadastro['dia_cadastro'])
#
# cadastro['turma'] = '5G'
#
# print(cadastro)
#
# cadastro['modalidade'] = 'EAD'
# print(cadastro)
#
# cadastro.pop('turma') # retira um item especifico
# print(cadastro)
#
# print(cadastro.items()) #retorna lista de chave-valor
#
# print(cadastro.keys()) # só retorna as chaves
# print(cadastro.values()) # só retorna os valores
#
# for chaves in cadastro.keys():  #for in cadastro.values():
#     print(cadastro[chaves])     #print(valores)

# ----------------Uso de função----------------
# def media():
#     nota_1 = float(input("Digite a 1° nota: "))
#     nota_2 = float(input("Digite a 2° nota: "))
#
#     print(f"Media = {(nota_1 + nota_2) / 2}")
#
#
# while True:
#     print('1. Calcular media')
#     print('2. Sair')
#
#     choice = int(input("Digite a opcao: "))
#
#     if choice == 1:
#         media()
#     elif choice == 2:
#         break
#     else:
#         print("Opcao invalida")

lista = [
    {'aluno': 'Pivas', "nota_1": 10, "nota_2": 20},
    {'aluno': 'Dodas', "nota_1": 100, "nota_2": 200}
]

print(lista)

aux = int(input("Informe a quantidade de alunos: "))

for count in range(1, aux+1):
    aluno = input('Digite o nome do aluno: ')
    nota_1 = float(input('Digite a primeira nota: '))
    nota_2 = float(input('Digite a segunda nota: '))

    lista.append({'aluno': aluno, 'nota_1': nota_1, 'nota_2': nota_2})
    print('Aluno adicionado com sucesso!')

print(lista)