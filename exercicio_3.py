# --------------------------Exercicio 3 -----------------------------

veiculo = [
    {'marca': 'mercedes', 'modelo': 'coupe', 'preço': 336900.00}
]

# ------- Função comprar veiculos -------
def comprarVeiculo():
    comprarCarro = int(input('Digite o número do carro que deseja comprar: '))
    del veiculo[comprarCarro - 1]

#------- Função cadastrar veiculos -------
def cadastrarVeiculo():
    cadastrarVeic_marca = input('Digite a marca do carro que deseja cadastrar: ')
    cadastrarVeic_modelo = input('Digite o modelo do carro que deseja cadastrar: ')
    cadastrarVeic_preco = float(input('Digite o preço do veiculo que deseja cadastrar: '))

    veiculo.append({'marca': cadastrarVeic_marca, 'modelo': cadastrarVeic_modelo, 'preço': cadastrarVeic_preco})
    print('O veiculo foi cadastrado com sucesso!')

#------- Função listagem de veiculos -------
def listarVeiculos():
    for index, carro in enumerate(veiculo, start=1):
        print(f"{index} - {carro}")


#------- Loop da loja -------
while True:
    print('1. Comprar')
    print('2. Cadastar')
    print('3. Listar')
    print('4. Sair')

    escolha_Operacao = int(input("Digite a opcao: "))

    if escolha_Operacao == 1:
        comprarVeiculo()
    elif escolha_Operacao == 2:
        cadastrarVeiculo()
    elif escolha_Operacao == 3:
        listarVeiculos()
    else:
        break
